module fr.mc.projet4gestiondecontacts {
    requires javafx.controls;
    requires javafx.fxml;
    requires kernel;
    requires io;
    requires layout;


    opens fr.mc.projet4gestiondecontacts to javafx.fxml;
    exports fr.mc.projet4gestiondecontacts;
}