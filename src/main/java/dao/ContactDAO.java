package dao;

import fr.mc.projet4gestiondecontacts.Person;
import javafx.collections.ObservableList;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ContactDAO {

    private FichierTexte fichierTexte;

    public ContactDAO(File file){
        fichierTexte = new FichierTexte(file);
    }

    public List<Person> lire(){


        List<Person> contacts = new ArrayList<>();
        List<String> lignes = fichierTexte.lire();
        for (String ligne : lignes){
            contacts.add(stringToPersonne(ligne));
        }
    return contacts;
    }

    private Person stringToPersonne(String chaine) {
        List<String> parse;
        parse= Arrays.asList(chaine.split("\\|"));
        String nom = parse.get(0);
        String prenom = (parse.size()>1? parse.get(1):"");
        String dateNaissance = (parse.size()>2? parse.get(2):"");

    return new Person(nom, prenom);
    }


    public void sauver(ObservableList<Person> contacts){
        List<String> lignes = new ArrayList<>();
        for (Person personne : contacts){
            lignes.add(toLigne(personne));
        }
        fichierTexte.ecrire(lignes);
    }

    private String toLigne(Person personne) {
        String ligne;
        ligne = personne.getFirstName() + "|" + personne.getLastName() + "|" + personne.getStreet() + "|" + personne.getPostalCode() + "|" + personne.getCity() + "|" + personne.getBirthday();
    return ligne;
    }
}
