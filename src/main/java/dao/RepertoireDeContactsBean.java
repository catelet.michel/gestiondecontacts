package dao;

import fr.mc.projet4gestiondecontacts.MainApp;
import fr.mc.projet4gestiondecontacts.Person;
import fr.mc.projet4gestiondecontacts.RootLayoutController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.MenuItem;

import java.io.File;
import java.util.ArrayList;
import java.util.function.Predicate;

public class RepertoireDeContactsBean {





    private ObservableList<Person> allContacts;

    private FilteredList<Person> filteredList;

    private SortedList<Person> sortedList;

    private File file;

    private ContactDAO contactDAO;

    private Person personSelected;

    private boolean saved;

    private Person personSearched;




    public ObservableList<Person> getAllContacts() {
        return allContacts;
    }


    public RepertoireDeContactsBean(File file){
        this.personSearched = new Person("","");
        this.file=file;
        this.contactDAO=new ContactDAO(file);
        this.allContacts= FXCollections.observableArrayList(this.contactDAO.lire());
        saved = true;
        //this.filteredList= new FilteredList(allContacts);
        this.filteredList= new FilteredList<>(allContacts,null);

    }

    protected void setPredecate(Predicate<Person>predicate){


    }

//public void (Predicate<? super Person>) predicate(String nom, String prenom){

    //}

    public void getPredicate() {
        //return Predicate;
    }

    public void deleteFichier(File file){
        try{
            if(file.delete()){
                System.out.println(file.getName() + " est supprimé.");

            }else{
                System.out.println("Opération de suppression echouée");
            }
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private Object lire() {
    return null;
    }

    public void ouvrirFichier(File file){

        ContactDAO contacts = new ContactDAO(file);
        contacts.lire();
        allContacts.addAll(contacts.lire());

    }



    public void sauvegarderFichier(File file,ObservableList<Person> listeContacts){

        ContactDAO contacts = new ContactDAO(file);
        contacts.sauver(listeContacts);
    }


    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public FilteredList<Person> getFilteredList() {
        return filteredList;
    }

    public SortedList<Person> getSortedList() {
        return sortedList;
    }
}
