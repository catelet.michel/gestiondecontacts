package fr.mc.projet4gestiondecontacts;

import dao.ContactDAO;
import dao.RepertoireDeContactsBean;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;

/**
 * The controller for the root layout. The root layout provides the basic
 * application layout containing a menu bar and space where other JavaFX
 * elements can be placed.
 *
 * @author Marco Jakob
 */
public class RootLayoutController {

    public MenuItem boutonPrint;
    public MenuItem boutonSave;
    public MenuItem boutonSaveAs;
    // Reference to the main application
    private MainApp mainApp;

    private static final String INITIAL_DIRECTORY = "C:\\Users\\Michel\\Documents\\Afpa\\ProjetsFX\\Projet4_GestionDeContacts\\";






    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        boutonSave.setDisable(true);
        boutonPrint.setDisable(true);
        boutonSaveAs.setDisable(true);
    }


    //private RepertoireDeContactsBean monRepertoireDeContactBean;


    public MenuItem getBoutonPrint() {
        return boutonPrint;
    }

    public void setBoutonPrint(MenuItem boutonPrint) {
        this.boutonPrint = boutonPrint;
    }

    /**
     * Creates an empty address book.
     */

    @FXML
    private void handleNew() {

      boutonSaveAs.setDisable(false);
        boutonPrint.setDisable(false);

        File file = new File(INITIAL_DIRECTORY);


        //RepertoireDeContactsBean
        if (file != null){

            this.mainApp.setRepertoireBean(new RepertoireDeContactsBean(file));
            this.mainApp.getPrimaryStage().setTitle(file.getPath());
            this.mainApp.showPersonOverview();

            //getPersonData().clear();
            //showPersonOverview();
            //getPrimaryStage().setTitle(file.getPath());


        }

    }

    @FXML
    private void handleDelete(){

        System.out.println("test");
        File monFicher=mainApp.getRepertoireBean().getFile();

        if (monFicher != null)
            mainApp.getRepertoireBean().deleteFichier(monFicher);
            mainApp.getRepertoireBean().getAllContacts().clear();
            mainApp.getPrimaryStage().setTitle("AdressApp");


            //mainApp.getRootLayout().setCenter(null);
            //mainApp.getPrimaryStage().setTitle("AdressApp");

        //File monchifier=this.monRepertoireDeContactBean.getFile();
        //monRepertoireDeContactBean.deleteFichier(monchifier);
        //mainApp.getPersonData().clear();
        //mainApp.getRootLayout().setCenter(null);
        //mainApp.getPrimaryStage().setTitle("AdressApp");

    }

    /**
     * Opens a FileChooser to let the user select an address book to load.
     */

    @FXML
    private void handleOpen() {

        FileChooser fileChooser = getFileChooser();

        // Show open file dialog
        fileChooser.setTitle("Ouvrir un Fichier...");
        fileChooser.setInitialDirectory(new File(INITIAL_DIRECTORY));
        File file = fileChooser.showOpenDialog(mainApp.getPrimaryStage());

        if (file != null) {


            this.mainApp.setRepertoireBean(new RepertoireDeContactsBean(file));
            this.mainApp.getRepertoireBean().ouvrirFichier(file);
                boutonPrint.setDisable(false);
                boutonSave.setDisable(false);
            boutonSaveAs.setDisable(false);

            this.mainApp.getPrimaryStage().setTitle(file.getPath());
            this.mainApp.showPersonOverview();


            //this.monRepertoireDeContactBean = new RepertoireDeContactsBean(file);
            //mainApp.getPersonData().clear();
            //ContactDAO contacts = new ContactDAO(file);
            //contacts.lire();
            //mainApp.getPersonData().addAll(contacts.lire());
            //mainApp.getPrimaryStage().setTitle(file.getPath());
            //mainApp.showPersonOverview();

        }
    }





    /**
     * Saves the file to the person file that is currently open. If there is no
     * open file, the "save as" dialog is shown.
     */

    @FXML
    private void handleSave() {

        //File monchifier=this.monRepertoireDeContactBean.getFile();
        File monchifier = mainApp.getRepertoireBean().getFile();

        if (monchifier != null) {

            // Make sure it has the correct extension
            if (!monchifier.getPath().endsWith(".txt")) {
                monchifier = new File(monchifier.getPath() + ".txt");
            }

            mainApp.getRepertoireBean().sauvegarderFichier(monchifier, mainApp.getRepertoireBean().getAllContacts());


         } else {
            handleSaveAs();
        }
    }



    /**
     * Opens a FileChooser to let the user select a file to save to.
    */
   @FXML
   private void handleSaveAs() {
       FileChooser fileChooser = getFileChooser();

       // Show save file dialog
       fileChooser.setTitle("Sauvegarder un Fichier...");
       fileChooser.setInitialDirectory(new File(INITIAL_DIRECTORY));
       File file = fileChooser.showSaveDialog(mainApp.getPrimaryStage());

        if (file != null) {
            // Make sure it has the correct extension
            if (!file.getPath().endsWith(".txt")) {
                file = new File(file.getPath() + ".txt");
            }

            mainApp.getRepertoireBean().sauvegarderFichier(file, mainApp.getRepertoireBean().getAllContacts());
            mainApp.getRepertoireBean().setFile(file);
            mainApp.getPrimaryStage().setTitle(file.getPath());

            //ContactDAO contacts = new ContactDAO(file);
            //contacts.sauver(mainApp.getPersonData());

        }
        boutonSave.setDisable(false);
    }

    private FileChooser getFileChooser() {
        FileChooser fileChooser = new FileChooser();

        // Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);
        return fileChooser;
    }

    @FXML

    private void handlePrint() throws IOException {
        Pdf pdf = new Pdf();
    }




    /**
     * Opens an about dialog.
     */
    @FXML
    private void handleAbout() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("AddressApp");
        alert.setHeaderText("About");
        alert.setContentText("Author: Marco Jakob\nWebsite: http://code.makery.ch");

        alert.showAndWait();
    }

    /**
     * Closes the application.
     */
    @FXML
    private void handleExit() {
        Platform.exit();
    }
}
