package fr.mc.projet4gestiondecontacts;

import dao.RepertoireDeContactsBean;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class MainApp extends Application {
    
    private Stage primaryStage;
    private BorderPane rootLayout;

    private RepertoireDeContactsBean repertoireBean;

    /**
     * The data as an observable list of Persons.
     */
    private ObservableList<Person> personData = FXCollections.observableArrayList();
    @Override
    public void start(Stage primaryStage) {
        
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("AdressApp");

        // Set the application icon.
        //
        this.primaryStage.getIcons().add(new Image("file:C:\\Users\\Michel\\Documents\\Afpa\\ProjetsFX\\Projet4-GestionDeContacts\\src\\main\\resources\\fr\\mc\\projet4gestiondecontacts\\103287_adressbook_icon.png"));
        //String icone = MainApp.class.getResource("103287_adressbook_icon.png");

        //this.primaryStage.getIcons().add(new Image("file:MainApp.class.getResource(103287_adressbook_icon.png"));
        //C:\Users\Michel\Documents\Afpa\ProjetsFX\Projet4-GestionDeContacts\src\main\resources\fr\mc\projet4gestiondecontacts
        initRootLayout();

        // One ne charge plus la liste de depart pour l ouvrir plus tard en ouvrant un fichier
        //showPersonOverview();



    }

    /**
     * Shows the person overview inside the root layout.
     */
    public void showPersonOverview() {
        try{
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("PersonOverview.fxml"));
            AnchorPane personOverview = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.
            rootLayout.setCenter(personOverview);

            // Give the controller access to the main app.
            PersonOverviewController controller = loader.getController();
            controller.setMainApp(this);



        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Initializes the root layout.
     */
    private void initRootLayout() {
        try{
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();


            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);

            primaryStage.setScene(scene);
            primaryStage.setWidth(900);
            primaryStage.setHeight(600);
            // Give the controller access to the main app.
            RootLayoutController controller = loader.getController();
            controller.setMainApp(this);


            //rootLayout.setCenter(new Image("file:C:\\Users\\Michel\\Documents\\Afpa\\ProjetsFX\\Projet4-GestionDeContacts\\src\\main\\resources\\fr\\mc\\projet4gestiondecontacts\\Depositphotos_37887225_xl-800x500.jpg"));

            primaryStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * Returns the main stage.
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch();
    }


    /**
     * Constructor
     */
    public MainApp() {
//        personData.add(new Person("Hans", "Muster"));
//        personData.add(new Person("Ruth", "Mueller"));
//        personData.add(new Person("Heinz", "Kurz"));
//        personData.add(new Person("Cornelia", "Meier"));
//        personData.add(new Person("Werner", "Meyer"));
//        personData.add(new Person("Lydia", "Kunz"));
//        personData.add(new Person("Anna", "Best"));
//        personData.add(new Person("Stefan", "Meier"));
//        personData.add(new Person("Martin", "Mueller"));
    }


    /**
     * Returns the data as an observable list of Persons.
     * @return
     */
    public ObservableList<Person> getPersonData() {
        return personData;
    }


    /**
     * Opens a dialog to edit details for the specified person. If the user
     * clicks OK, the changes are saved into the provided person object and true
     * is returned.
     *
     * @param person the person object to be edited
     * @return true if the user clicked OK, false otherwise.
     */
    public boolean showPersonEditDialog(Person person) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("PersonEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Person");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            PersonEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setPerson(person);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public RepertoireDeContactsBean getRepertoireBean() {
        return repertoireBean;
    }

    public void setRepertoireBean(RepertoireDeContactsBean repertoireBean) {
        this.repertoireBean = repertoireBean;
    }

    public void setRootLayout(BorderPane rootLayout) {
        this.rootLayout = rootLayout;
    }

    public BorderPane getRootLayout() {
        return rootLayout;
    }
}